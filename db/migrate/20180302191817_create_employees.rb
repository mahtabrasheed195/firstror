class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.text :name
      t.text :department

      t.timestamps
    end
  end
end
